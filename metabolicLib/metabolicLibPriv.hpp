/*
 *  metabolicLibPriv.hpp
 *  metabolicLib
 *
 *  Created by Panos Giotis on 5/26/16.
 *  Copyright © 2016 BRI. All rights reserved.
 *
 */

/* The classes below are not exported */
#pragma GCC visibility push(hidden)

class metabolicLibPriv
{
	public:
		vector<float> internalAnalysis(InterConnectionModule * ICM);
        vector<float> frame_merge(vector<float> input);
        vector<float> medianFilter(vector<float> x);


};

#pragma GCC visibility pop
