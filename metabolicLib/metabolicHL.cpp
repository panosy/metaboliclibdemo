//
//  metabolicHL.cpp
//
//  metabolicHL
//  This file is part of the C++ metabolic testing framework
//  Breath Research Inc.
//
//  Created by Panagiotis Giotis
//  Copyright (c) 2016 BRI. All rights reserved.
//


#include "metabolicHL.h"

using namespace std;



vector<float> metabolicHL::findCandidates(vector<float> Intensity, vector<float> BPM, vector<float> HR)
{
    
    if(PRINT_DEBUG_OUTPUT) cout << "******************************************************************************************";
    if(PRINT_DEBUG_OUTPUT) cout << "\n*** Candidate Selection Module (plus trimming)...";

    // minMax vector length
    unsigned long L = Intensity.size();
    
    // Output vectors
    vector<float> outputVT_IDX, outputVT_IDX_v2;
    
    // Declare the intermediate vectors for the minMax method for all 3 metrics
    vector<float> minInt, maxInt, minIntSl, maxIntSl, minIntSl2, maxIntSl2;
    // BPM
    vector<float> minBPM, maxBPM, minBPMSl, maxBPMSl, minBPMSl2, maxBPMSl2;
    // HR
    vector<float> HRSl, HRSl2, minHR, maxHR, minHRSl, maxHRSl, minHRSl2, maxHRSl2; // Set HR as existent and ZERO, to be replaced with sensor values
    
    
    // Check if we have no heart rate input from sensor and if so set to zero-vector
    if ( HR.size() != L )
    {
        vector<float> HR_zeros (L,0);
        HR = HR_zeros;
    }
    
    // Declare the subtotal minMax vectors
    vector<float> minSum(L,0);
    vector<float> minSumSl(L,0);
    vector<float> minSumSum(L,0);
    vector<float> maxSumSum(L,0);
    vector<float> totalSum (L,0);
    
    
    // Declare and calculate the 1st & 2nd degree derivatives
    vector<float> slopeIntensity = diff(Intensity);
    vector<float> slopeIntensity2 = diff(slopeIntensity);
    vector<float> slopeBPM = diff(BPM);
    vector<float> slopeBPM2 = diff(slopeBPM);
    vector<float> slopeHR = diff(HR);
    vector<float> slopeHR2 = diff(slopeHR);
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // minMax vector preparation for all metrics
    if(PRINT_DEBUG_OUTPUT) cout << "\n*** minMax vector preparation...";

    // Intensity
    findMinMaxVec(Intensity, &minInt, &maxInt);
    findMinMaxVec(slopeIntensity, &minIntSl, &maxIntSl);
    // tmp to shift and insert minimum
    vector<float> tmp (slopeIntensity2.begin() + 1, slopeIntensity2.end());
    tmp.push_back( *min_element(slopeIntensity2.begin(), slopeIntensity2.end()) );
    findMinMaxVec(tmp, &minIntSl2, &maxIntSl2);
    
    // BPM
    findMinMaxVec(BPM, &minBPM, &maxBPM);
    findMinMaxVec(slopeBPM, &minBPMSl, &maxBPMSl);
    
    // HR
    findMinMaxVec(HR, &minHR, &maxHR);
    findMinMaxVec(slopeHR, &minHRSl, &maxHRSl);
    
    // Create shifted vector for minMax detection
    vector<float> slopeHR2Shifted (slopeHR2.begin()+1, slopeHR2.end());
    slopeHR2Shifted.push_back( *min_element(slopeHR2.begin(), slopeHR2.end()) );
    findMinMaxVec(slopeHR2Shifted, &minHRSl2, &maxHRSl2);
    
    // Shifted Intensity 2nd derivative
    vector<float> slIntensity2Exp (slopeIntensity2.begin()+1, slopeIntensity2.end());
    slIntensity2Exp.push_back(*min_element(slopeIntensity2.begin(), slopeIntensity2.end()));
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // MinMax sum subtotal calculations
    if(PRINT_DEBUG_OUTPUT) cout << "\n*** minMax subtotal calculations running...";

    for ( unsigned long i=0; i < L ; i++ )
    {
        minSum[i] = minBPM[i] + minInt[i] + minHR[i];
        minSumSl[i] = minBPMSl[i] + minIntSl[i] + minHRSl[i];
    }
    
    for ( unsigned long i=0; i < L ; i++ )
    {
        minSumSum[i] = 15 * minSum[i] + 4 * minSumSl[i];
        maxSumSum[i] = maxBPM[i] + maxInt[i] + maxHR[i] + maxBPMSl[i] + maxIntSl[i] + maxHRSl[i] + 3*maxHRSl2[i];
    }
    
    
    // Create shifted up and down vectors for maxSumSum, to be used in totalSum
    vector<float> maxSumSumShiftedUp (maxSumSum.begin(), maxSumSum.end()-1);
    maxSumSumShiftedUp.insert(maxSumSumShiftedUp.begin(), 0);
    vector<float> maxSumSumShiftedDown (maxSumSum.begin()+1, maxSumSum.end());
    maxSumSumShiftedDown.push_back(0);
    
    
    // Add the shifted maximums and the 2nd degree derivative of the Intensity
    for ( unsigned long i=1; i < L ; i++ )
    {
        //totalSum[i] = minSumSum[i] + maxSumSum[i+1] + maxSumSum[i-1] + + 25*maxIntSl2[i]; //-5maxSumSum
        totalSum[i] = minSumSum[i] + maxSumSumShiftedUp[i] + maxSumSumShiftedDown[i] + 30*maxIntSl2[i]; //-5maxSumSum /// WAS +25!
    }
    
    // DISABLED, EXPERIMENTAL FUNCTION, CHECK TO ENABLE
    // Adding the shifted max sums
    //    for ( unsigned long i=1; i < L ; i++ )
    //    {
    //        totalSum[i] = 10 * totalSum[i] * slIntensity2Exp[i] ; // Inverse sign (?) for upcoming minEqual function AND bias with intensity 2nd slope
    //    }
    
    // Print out intermediate point system vectors
    if (PRINT_DEBUG_OUTPUT)
    {
        printVec(minSum);
        printVec(minSumSl);
        printVec(minSumSum);
        printVec(maxSumSum);
        printVec(maxSumSumShiftedUp);
        printVec(maxSumSumShiftedDown);
        
    }
        
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Hard trimming off the beginning and end of minMax
    
    // Backup the unprocessed, untrimmed totalSum
    vector<float> totalSum_original (totalSum.size(),0);
    totalSum_original = totalSum;
    
    if(PRINT_DEBUG_OUTPUT) cout << "\n*** HardTrimming and candidate selection running...";

    
    if (hardTrimFlag)
    {
        totalSum = hardTrim(totalSum, ICM->sessionTrim, ICM->sessionCategory);
    }
    
    if (PRINT_DEBUG_OUTPUT)
    {
        cout << "\nOriginal untrimmed totalsum vector for candidate selection:";
        printVec(totalSum_original);
        cout << "Trimmed totalsum vector for candidate selection:";
        printVec(totalSum);
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Calculate the maximum peaks for the Primary approach of the minMax
    outputVT_IDX = findMinMaxVal (totalSum, 1, 2, 1, 1);
    // findMinMaxVal(vector<float> input, int direction, int numberOfCandidates, int findMaximumsFlag, int offset);
    
    
    // Print out the results for checking
    // Print the minMax Vectors
    if (PRINT_DEBUG_OUTPUT)
    {
        cout << "\n<> Unprocessed Original TotalSum Vector:";
        printVec(totalSum_original);
        
        cout << "<> Processed TotalSum Vector after trimming and filtering:";
        printVec(totalSum);
        
        cout<<"<> Original Output VT IDX Vector:";
        printVec(outputVT_IDX);
    }
    
    // Check for 1 candidate-result
    if (outputVT_IDX.size() == 1)
    {
        totalSum[outputVT_IDX[0] - 1] = 0;
        vector<float> temp = findMinMaxVal (totalSum, 1, 1, 1, 1);
        if (temp.size() > 0 )
        {
            // Sole main lobe, exctract 2 adjacent candidates from non-zero values
            if (PRINT_DEBUG_OUTPUT) cout << "*-> One threshold found, injecting second pass approach threshold (1-lobe)";
            outputVT_IDX.push_back (temp[0]);
        }
        else
        {
            // Inject element next to sole-candidate, due to zero-level adjacent thresholds [extreme case scenario]
            if (PRINT_DEBUG_OUTPUT) cout << "*-> One threshold found, injecting adjacent threshold (1-lobe and zeroes)";
            outputVT_IDX.push_back(outputVT_IDX[0]+1);
        }
    }
    
    // Link the IDX to real-life time sorted values for VT1 & VT2
    sort(outputVT_IDX.begin(), outputVT_IDX.end());
    
    // Primary VT output
    for (int i=0 ; i < outputVT_IDX.size(); i++)
    {
        outputVT_IDX[i] = (outputVT_IDX[i]) * ICM->sessionTrim;
    }
    
    cout<<"\n<> [PRIMARY] Sorted Output VT IDX Vector converted to minutes:";
    printVec(outputVT_IDX);
    
    ////////////////////////////////////////////////////////////////////////
    // Calculate VT by alternative approach - Secondary VT output
    
    // Place min element at end to catch maximum at the end of the session
    slopeIntensity2.push_back(*min_element(slopeIntensity2.begin(), slopeIntensity2.end()));
    
    
    // Hard trim the limits
    if (hardTrimFlag)
    {
        slopeIntensity2 = hardTrim(slopeIntensity2, ICM->sessionTrim, ICM->sessionCategory);
    }
    
    
    // Find maximums
    outputVT_IDX_v2 = findMinMaxVal(slopeIntensity2, 1, 2, 1, 0); // Set 0 offset, max is 1 frame later
    
    // Sort and map to real-life time values
    sort(outputVT_IDX_v2.begin(), outputVT_IDX_v2.end());
    
    for (int i=0 ; i < outputVT_IDX_v2.size(); i++)
        outputVT_IDX_v2[i] = (outputVT_IDX_v2[i]) * ICM->sessionTrim;
    
    
    cout<<"\n<> [ALTERNATIVE] Sorted Output BRI_VT2, IDX Vector:";
    printVec(outputVT_IDX_v2);
    
    
    // Store in ICM structure
    ICM->VTalternativeApproach = outputVT_IDX_v2;
    
    
    return outputVT_IDX;
}



vector<float> metabolicHL::hardTrim ( vector<float> x, int sessionTrim, int sessionCategory )
{

    unsigned long hardLeftLimit;// = (ICM->sessionTrim==1) ? 5 : 3;
    unsigned long hardRightLimit;
    int cooldownFlag = 1;

    long L = x.size();
    // Convert to minutes to do the separation
    L = L * sessionTrim;
    
    cout << "\n\n L = " << L;
    if (L <= 11)
    {
        hardLeftLimit = 2;
        hardRightLimit = L - cooldownFlag;
        cout << "\n\n Length category 1";
    }
    else if (L<=21)
    {
        hardLeftLimit = 3; // was 4
        hardRightLimit = L - 2 - cooldownFlag; // was -2
        cout << "\n\n Length category 2";

    }
    else
    {
        hardLeftLimit = 5 - cooldownFlag;
        hardRightLimit = L - 4 - cooldownFlag;
    }
    
    
    // Convert limits back to actual vector lengths
    hardLeftLimit = (unsigned long)ceil(hardLeftLimit/sessionTrim);
    hardRightLimit = (unsigned long)round(hardRightLimit/sessionTrim);
    

    if (PRINT_DEBUG_OUTPUT)
    {
        cout << "\n*** Hard Trimming & Session Length Details" << endl;
        cout<<"<> Session length is " << x.size()*sessionTrim << " minutes. Session is in lengthCategory #" << sessionCategory << ", sessionTrim is " << sessionTrim << endl;
        cout << "<> Primary totalsum size is: " << x.size() << ". Trimming below indice/minute: " << hardLeftLimit<<"/"<< hardLeftLimit*sessionTrim << ", above indice/minute: " << hardRightLimit << "/" << hardRightLimit*sessionTrim << "." << endl;

    }


    if (hardLeftLimit < 1) hardLeftLimit = 1;
    if (hardRightLimit > x.size()-1 ) hardRightLimit = x.size()-1;
    
    for ( unsigned long i=0; i < x.size() ; i++ )
    {
        if (i<=hardLeftLimit)
        {
            x[i] = 0;
        }
        if (i>=hardRightLimit)
        {
            x[i] = 0;
        }
    }
    
    // Review the last element setting
    // Set the last element of totalSum equal to the minimum one to avoid false peak in the end
    //totalSum[totalSum.size()-1] = *min_element(totalSum.begin(), totalSum.end());
    // OR set the last equal to the previous to last
    //totalSum[totalSum.size()-1] = totalSum[totalSum.size()-2];
    // OR SET TO 0 (exp)
    //x[L-1] = 0; // Redundant if included in prior limit check
    
    return x;
}




vector<float> metabolicHL::findMinMaxVal(vector<float> x, int direction, int numberOfCandidates, int findMaximumsFlag, int offset)

{
    // If direction is 0 scan from left to right, else right to left
    // If findMaximumsFlag is 1 return the indices of maximums, else of minimums
    // Offset is added to the position for real-life compensation (no C++ 0-index vs. Matlab)
    
    unsigned long L = x.size();
    vector<float> valMin, valMax;
    vector<float> posMin, posMax;
    
    
    if (direction == 0) // Scan from left to right
        
    {
        for ( unsigned long i = 1 ; i < L-2 ; i++ )
            // Find minimums and indices
            if( (x[i] < x[i-1]) && (x[i] < x[i+1]) && (valMin.size() < numberOfCandidates) )
            {
                valMin.push_back( x[i] );
                posMin.push_back( i + offset );
            }
        // Find maximums and indices
            else if( (x[i] > x[i-1]) && (x[i] > x[i+1]) && (valMax.size() < numberOfCandidates) )
            {
                valMax.push_back( x[i] );
                posMax.push_back( i + offset );
            }
    }
    
    else // Scan from right to left
    {
        for ( unsigned long i = L-2 ; i > 0 ; i-- )
        {
            // Find minimums and indices
            if( (x[i] < x[i-1]) && (x[i] <= x[i+1]) && (valMin.size() < numberOfCandidates) )
            {
                valMin.push_back( x[i] );
                posMin.push_back( i + offset );
            }
            // Find maximums and indices
            else if( (x[i] > x[i-1]) && (x[i] >= x[i+1]) && (valMax.size() < numberOfCandidates) )
            {
                valMax.push_back( x[i] );
                posMax.push_back( i + offset );
                
            }
        }
        
    }
    
    if (findMaximumsFlag)
        return posMax;
    else
        return posMin;
    
}

void metabolicHL::findMinMaxVec(vector<float> x, vector<float> * minVec, vector<float> * maxVec)
{
    
    unsigned long L = x.size();
    minVec->push_back(0);
    maxVec->push_back(0);
    
    
    
    for (int i = 1 ; i <= L-2 ; i ++)
    {
        if ((x[i] < x[i-1]) && (x[i] < x[i+1]) )
        {
            minVec->push_back(1);
        }
        else
        {
            minVec->push_back(0);
        }
        
        if ((x[i] > x[i-1]) && (x[i] > x[i+1]) )
        {
            maxVec->push_back(1);
        }
        else
        {
            maxVec->push_back(0);
        }
        
    }
    
    minVec->push_back(0);
    maxVec->push_back(0);
}

vector<float> metabolicHL::normalizeVec(vector<float> input)
{
    float maxElement = *max_element(input.begin(), input.end());
    for (int i=0 ; i< input.size(); i++)
    {
        input[i] /= maxElement;
    }
    
    return input;
}


void metabolicHL::printVec(vector<float> input)
{
    cout << "\nVector Contents: ";
    
    for (int i=0 ; i<input.size(); i++)
    {
        cout << input[i] << "  ";
    }
    cout << endl;
}


vector<float> metabolicHL::diff(vector<float> input)
{
    // Initialize vectors with 0 in the beggining for keeping equal steady size
    vector<float> output (1,0);
    
    // Diff
    for (int i=1; i<input.size() ; i++)
    {
        output.push_back(input[i]-input[i-1]);
    }
    
    return output;
}



vector<float> metabolicHL::trimVector(vector<float> input)
{
    
    // Frametrim is 2*sessionTrim. It's 2 for 1-minute, 4 for 2-minute analysis division (Number of Frames per block)
    int frameTrim = ICM->sessionTrim * 2;
    // Length of input vector
    long L = input.size();
    // Allocate output vector
    vector<float> output;
    
    // Fill-in output vector according to frameTrim/sessionTrim
    for (int i = frameTrim-1 ; i < L; i += frameTrim)
    {
        output.push_back(input[i]);
    }
    
    
    // If input length is odd, add the last element at the end to compensate for the frame lack
    if (L % frameTrim != 0) // length is odd
    {
        output.push_back(input.back());
        
    }
    
    return output;
    
}

vector<float> metabolicHL::stringVector2Float (vector<string> inputStringVector)
{
    vector<float> outputFloatVector;
    
    for (int i=0; i<inputStringVector.size() ; i++ )
    {
        float s = boost::lexical_cast<float>(inputStringVector[i]);
        outputFloatVector.push_back(s);
    }
    
    return outputFloatVector;
};

void metabolicHL::writeCSVOutput (vector<float> VT, vector<float> VTalternativeApproach, string outputFileName )
{
    ofstream myfile;
    
    // Open/Create the output file
    myfile.open (outputFileName, ios::out);
    
    cout << endl;
    
    // Main minMAX VT approach write-out. Write -1 if there are no thresholds detectd
    switch (VT.size()) {
        case 0:
        {
            myfile << "-1\n";
            cout<<"*** Error in candidate selection, no candidates were detected for primary, writing -1 ***" << endl;
            
            break;
        }
        case 1:
        {
            myfile << VT[0] << "\n";
            cout<<"*** Only one candidate detected through primary approach..." << endl;
            break;
            
        }
        case 2:
        {
            myfile << VT[0] << "," << VT[1] << "\n";
            cout<<"*** Two candidates succesfully detected through primary approach..." << endl;
            
            break;
        }
        case 3:
        {
            myfile << VT[0] << "," << VT[1] << "," << VT[2] << "\n";
            cout<<"*** Three candidates succesfully detected through primary approach..." << endl;
            break;
        }
            
        default:
            break;
    }
    
    
    // Alternative VT results, to be used in case primary fails
    switch (VTalternativeApproach.size()) {
        case 0:
        {
            myfile << "-1";
            cout<<"*** Error in alternative candidate selection, no candidates were detected for alternative, writing -1 ***" << endl;
            break;
        }
        case 1:
        {
            cout<<"*** Warning: Only one candidate detected for alternative!" << endl;
            myfile << VTalternativeApproach[0];
            break;
            
        }
        case 2:
        {
            myfile << VTalternativeApproach[0] << "," << VTalternativeApproach[1];
            cout<<"*** Two candidates succesfully detected through alternative approach..." << endl;
            break;
        }
        case 3:
        {
            myfile << VTalternativeApproach[0] << "," << VTalternativeApproach[1] << "," << VTalternativeApproach[2];
            cout<<"*** Three candidates succesfully detected through alternative approach..." << endl;
            break;
        }
            
        default:
            break;
    }
    
    cout<<"*** Output CSV file succesfully written..." << endl;
    
    
    // Write code version to file
    myfile << "\n" << metabolicHLVersion;
    
    // Close the CSV file
    myfile.close();
    
}


