/*
 *  metabolicLib.hpp
 *  metabolicLib
 *
 *  Created by Panos Giotis on 5/26/16.
 *  Copyright © 2016 BRI. All rights reserved.
 *
 */

#ifndef metabolicLib_
#define metabolicLib_

/* The classes below are exported */
#pragma GCC visibility push(default)

#include "parameters.h"

class metabolicLib
{
	public:
		void analyzeSession(std::vector<float> Intensity, std::vector<float> BPM, std::vector<float> HR, std::vector<float> BRI_VT);
};

#pragma GCC visibility pop
#endif
