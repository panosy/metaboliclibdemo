//
//  dataPool.cpp
//
//  metabolicHL
//  This file is part of the C++ metabolic testing framework
//  Breath Research Inc.
//
//  Created by Panagiotis Giotis
//  Copyright (c) 2016 BRI. All rights reserved.
//


#include "InterConnectionModule.h"

void InterConnectionModule::getSessionCategory(int sessionLengthInFrames, int & sessionCategory, int & sessionTrim)

{
    
    if (sessionLengthInFrames < 30)
    {
        sessionCategory = 1;
        sessionTrim = 1;
    }
    else if (sessionLengthInFrames < 40)
    {
        sessionCategory = 2;
        sessionTrim = 2;
    }
    else
    {
        sessionCategory = 3;
        sessionTrim = 2;

    }

}
