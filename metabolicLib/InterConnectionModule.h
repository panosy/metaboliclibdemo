//
//  dataPool.h
//
//  metabolicHL
//  This file is part of the C++ metabolic testing framework
//  Breath Research Inc.
//
//  Created by Panagiotis Giotis
//  Copyright (c) 2016 BRI. All rights reserved.
//


#ifndef INTERCONNECTION_MODULE
#define INTERCONNECTION_MODULE

#include "parameters.h"


using namespace std;

class InterConnectionModule

{
    
public:
    
    // Constructor and destructor
    InterConnectionModule(){}
    ~InterConnectionModule(){}
    
    // Aux function for session Category according to session length
    void getSessionCategory(int sessionLengthInFrames, int & sessionCategory, int & sessionTrim);
    
    // Class variables
    vector<float> intensityVector;
    vector<float> HRVector;
    vector<float> bpmVector;
    
    vector<float> VT, VTalternativeApproach;
    
    string inputFilename, outputFileName;
    
    float sR;
    char *fileName;
    int sessionCategory, sessionTrim, sessionDurationMins;
    
private:
    
};

#endif /* defined(__metabolicHL__dataPool__) */

