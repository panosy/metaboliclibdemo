//
//  metabolicHL.h
//
//  metabolicHL
//  This file is part of the C++ metabolic testing framework
//  Breath Research Inc.
//
//  Created by Panagiotis Giotis
//  Copyright (c) 2016 BRI. All rights reserved.
//


#ifndef ____metabolicHL__
#define ____metabolicHL__

#include "parameters.h"


class metabolicHL

{
    
public:
    
    // Constructor and destructor
    metabolicHL(InterConnectionModule * ICM_in)
    {
        ICM = ICM_in;
    };
    
    ~metabolicHL(){};

    
    // Function for candidate detection and selection with the minMax module
    vector<float> findCandidates(vector<float> Intensity, vector<float> BPM, vector<float> HR);
    
    // Aux Functions
    
    // If findMaximumsFlag is 0, returns vector of positions of minimums, else of maximums
    // If offset is 1 it adds 1 to the end indices to compensate for c++/matlab indexing
    vector<float> findMinMaxVal(vector<float> x, int direction, int numberOfCandidates, int findMaximumsFlag, int offset);
    
    vector<float> hardTrim ( vector<float> x, int sessionTrim, int sessionCategory );

    vector<float> trimVector(vector<float> inVec);
    vector<float> diff(vector<float> input);
    vector<float> normalizeVec(vector<float> input);
    vector<float> stringVector2Float (vector<string> inputStringVector);

    void printVec(vector<float> input);
    void findMinMaxVec(vector<float> input, vector<float> * minVec, vector<float> * maxVec);
    void writeCSVOutput (vector<float> VT, vector<float> VTalternativeApproach, string outputFileName );
    
    InterConnectionModule * ICM;
    
};

#endif /* defined(____metabolicHL__) */


