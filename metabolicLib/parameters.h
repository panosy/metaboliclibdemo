//
//  parameters.h
//
//  metabolicHL
//  This file is part of the C++ metabolic testing framework
//  Breath Research Inc.
//
//  Created by Panagiotis Giotis
//  Copyright (c) 2016 BRI. All rights reserved.
//


#ifndef _parameters_h
#define _parameters_h


#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "math.h"
#include "InterConnectionModule.h"

//#include "libsndfile.h"
//#include "readWavObj.h"

#include <boost/algorithm/string.hpp>

#include <iterator>     // ostream_operator
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>


//# define sessionTrim 2 // Redefined as dynamic based on session duration

# define hardTrimFlag 1 // Turn on hard trimming of the session boundaries
# define FRAME_SIZE 30 //in seconds
# define PRINT_DEBUG_OUTPUT 1 // Debug flag for console output
# define PRINT_LL_DEBUG_OUTPUT 0 // Debug flag for low layer console output
# define DROPBOX 1 // Flag to enable read-in from Dropbox for result confirmation of the test set
# define FRAME_MERGING_10_TO_30_SECS 1 // Enabled merging 3 frames in one (10sec -> 30 second frames)

#define metabolicHLVersion "v.1.2.0"

#ifndef PI
#define PI 3.141592653589793
#endif

#ifndef SQRT_2
#define SQRT_2  1.414213562373095
#endif


using namespace std;


#endif
