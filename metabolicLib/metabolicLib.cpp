/*
 *  metabolicLib.cpp
 *  metabolicLib
 *
 *  Created by Panos Giotis on 5/26/16.
 *  Copyright © 2016 BRI. All rights reserved.
 *
 */

#include <iostream>
#include "metabolicLib.hpp"
#include "metabolicLibPriv.hpp"
#include <metabolicHL.h>
#include "InterConnectionModule.h"

void metabolicLib::analyzeSession(std::vector<float> Intensity, std::vector<float> BPM, std::vector<float> HR, std::vector<float> BRI_VT)
{
	 metabolicLibPriv *theObj = new metabolicLibPriv;
    
    // Inteconnection module object
    InterConnectionModule  * ICM = new InterConnectionModule;
    
    ICM->intensityVector = Intensity;
    ICM->bpmVector = BPM;
    ICM->HRVector = HR;
    

    BRI_VT = theObj->internalAnalysis(ICM);
    
    delete theObj;
};

vector<float> metabolicLibPriv::internalAnalysis(InterConnectionModule * ICM)
{

    // MetabolicHL module object
    metabolicHL * mHL = new metabolicHL(ICM);

    //unsigned long pre_conversion_length = ICM->intensityVector.size();
    
    // Merge the frames and replace vectors
    ICM->intensityVector = frame_merge(ICM->intensityVector);
    //mHL->printVec(ICM->bpmVector);
    ICM->bpmVector = medianFilter(ICM->bpmVector);
    // mHL->printVec(ICM->bpmVector);
    
    ICM->bpmVector= frame_merge(ICM->bpmVector);
    ICM->HRVector = frame_merge(ICM->HRVector);
    
    // Get session length
    ICM->sessionDurationMins = round((float)ICM->intensityVector.size() / 6);
    
    // Get the session Category and sessionTrim here
    ICM->getSessionCategory((int)ICM->intensityVector.size(), ICM->sessionCategory, ICM->sessionTrim);
    
    // Enable to force a specific trimming 1/2, irrelevant of session size
    //ICM->sessionTrim = 1;
    
    // Trim the vectors according to our sessionTrim
    ICM->bpmVector = mHL->trimVector(ICM->bpmVector);
    ICM->intensityVector = mHL->trimVector(ICM->intensityVector);
    ICM->HRVector = mHL->trimVector(ICM->HRVector);
    
    // Normalize the Intensity and BPM vectors
    ICM->bpmVector = mHL->normalizeVec(ICM->bpmVector);


    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Run Candidate Extraction Module
    ICM->VT = mHL->findCandidates(ICM->intensityVector, ICM->bpmVector, ICM->HRVector);
    
    // Write candidates to CSV
    //mHL->writeCSVOutput(ICM->VT, ICM->VTalternativeApproach, ICM->outputFileName);
    
    return ICM->VT;
    
};

// Frame merge function to convert 10-sec frames to 30-sec frames
// Similar to 3-to-1 downsampling
vector<float> metabolicLibPriv::frame_merge(vector<float> input) {
    
    long L = input.size();
    
    vector<float> output;
    int limit = L%3;
    float sum = 0;
    
    for (int i=0; i<L-limit; i+=3)
    {
        sum = (float)((input[i] + input[i+1] + input[i+2]) / 3);
        output.push_back(sum);
    }
    
    // Compensate for extra elements at the end
    if (limit==1) output.push_back(input.back());
    if (limit==2) output.push_back( (input[input.size()-1]  + input[input.size()-2])/2);
    
    return output;
}

// Fixed-window median filter (length = 5)
vector<float> metabolicLibPriv::medianFilter(vector<float> x) {
    
    // Zero-pad
    long L = x.size();
    vector<float> pad(2, 0.0);
    vector<float> y(L,0.0);
    vector<float> temp = pad;
    
    temp.insert(temp.end(), x.begin(), x.end());
    temp.insert(temp.end(), pad.begin(), pad.end());
    x = temp;
    temp = pad;
    
    long  pin = 0;
    long  pend = L-1;
    
    while (pin < pend)
    {
        temp.assign(x.begin()+pin, x.begin()+pin+5 );
        std::sort(temp.begin(), temp.end());
        y[pin] = temp[ 2 ];
        pin++;
    }
    
    return y;
}

